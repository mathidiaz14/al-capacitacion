@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('helps.alert')
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Temas del curso "{{$curso->titulo}}"</h6>
                        </div>
                        <div class="col-6 text-right">
                        	<!-- Button trigger modal -->
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTeme">
								<i class="fa fa-plus"></i>
								Nuevo tema
							</button>

							<!-- Modal -->
							<div class="modal fade" id="addTeme" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
								<div class="modal-dialog" role="document">
							  		<div class="modal-content">
										<div class="modal-header">
							        		<h5 class="modal-title" id="exampleModalLabel">Nuevo tema</h5>

											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
							    		</div>
							    		<div class="modal-body text-left">
							      			<form action="{{url('admin/temario')}}" method="post" class="form-horizontal">
												@csrf
												<input type="hidden" name="curso" value="{{$curso->id}}">
												<div class="form-group">
													<label for="">Titulo</label>
													<input type="text" class="form-control" name="titulo" placeholder="Ingrese el titulo aqui" id="titulo">
												</div>
												<hr>
												<div class="row">
													<div class="col-6">
														<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
													</div>
													<div class="col-6 text-right">
														<button class="btn btn-primary">
															<i class="fa fa-save"></i>
															Guardar
														</button>			
													</div>
												</div>
											</form>
							    		</div>
							  		</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                @foreach($temas as $tema)
                                    <tr>
                                        <td>
                                            {{$tema->titulo}}
                                        </td>
                                        <td>
                                        	<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editarTema{{$tema->id}}">
												<i class="fa fa-edit"></i>
											</button>

											<!-- Modal -->
											<div class="modal fade" id="editarTema{{$tema->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
												<div class="modal-dialog" role="document">
											  		<div class="modal-content">
														<div class="modal-header">
											        		<h5 class="modal-title" id="exampleModalLabel">Editar tema</h5>

															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
											    		</div>
											    		<div class="modal-body text-left">
											      			<form action="{{url('admin/temario', $tema->id)}}" method="post" class="form-horizontal">
																@csrf
																@method('PATCH')
																<div class="form-group">
																	<label for="">Titulo</label>
																	<input type="text" class="form-control" name="titulo" value="{{$tema->titulo}}">
																</div>
																<hr>
																<div class="row">
																	<div class="col-6">
																		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
																	</div>
																	<div class="col-6 text-right">
																		<button class="btn btn-primary">
																			<i class="fa fa-save"></i>
																			Guardar
																		</button>			
																	</div>
																</div>
															</form>
											    		</div>
											  		</div>
												</div>
											</div>
                                        </td>
                                        <td>
                                            @include('helps.delete', ['id' => $tema->id, 'ruta' => url('admin/temario', $tema->id)])
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    
    <script>
        $('#addTeme').on('shown.bs.modal', function () {
          $('#titulo').trigger('focus')
        })
    </script>
@endsection