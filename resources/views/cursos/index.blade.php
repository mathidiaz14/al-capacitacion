@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('helps.alert')
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Cursos</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/cursos/create')}}" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                                Nuevo curso
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Nombre</td>
                                    <td>Pecio</td>
                                    <td>Carga horaria</td>
                                    <td>Temas</td>
                                    <td>Editar</td>
                                    <td>Eliminar</td>
                                </tr>
                                @foreach($cursos as $curso)
                                    <tr>
                                        <td>
                                            <img src="{{asset($curso->imagen)}}" alt="" width="100px">
                                        </td>
                                        <td>{{$curso->titulo}}</td>
                                        <td>${{$curso->precio}}</td>
                                        <td>{{$curso->carga_horaria}}</td>
                                        <td>
                                            <a href="{{url('admin/temario',$curso->id)}}" class="btn btn-primary">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{route('cursos.edit', $curso->id)}}" class="btn btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include('helps.delete', ['id' => $curso->id, 'ruta' => url('admin/cursos', $curso->id)])
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
