@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Editar Curso</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/cursos')}}" class="btn btn-secondary">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/cursos', $curso->id)}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    	@csrf
                    	@method('PATCH')
                    	<div class="row">
                    		<div class="col-12 col-lg-8">
                    			<div class="form-group">
		                    		<label for="">Titulo</label>
		                    		<input type="text" name="titulo" class="form-control" placeholder="Ingrese el titulo aquí" required="" autofocus="" value="{{$curso->titulo}}">
		                    	</div>
                    		</div>
                    		<div class="col-12 col-lg-4">
                    			<div class="form-group">
                    				<div class="label">Imagen</div>
                    				<img src="{{asset($curso->imagen)}}" alt="" width="100px">
                    				<input type="file" name="imagen" class="form-control" value="{{$curso->imagen}}">
                    			</div>
                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-12">
                    			<label for="">Descripción</label>
                    			<textarea name="descripcion" id="editor" rows="10" class="summernote form-control" required="" >{{$curso->descripcion}}</textarea>
                    		</div>
                    	</div>
                    	<br>
                    	<div class="row">
                    		<div class="col-12 col-md-4">
                    			<div class="form-group">
                    				<label for="">Modalidad</label>
                    				<select name="modalidad" id="" class="form-control">
                    					@if($curso->modalidad == "Virtual")
                    						<option value="Prepresencial">Presencial</option>
                    						<option value="Virtual" selected="">Virtual</option>
                    					@else
                    						<option value="Prepresencial" selected="">Presencial</option>
                    						<option value="Virtual">Virtual</option>
                    					@endif
                    				</select>
                    			</div>
                    		</div>
                    		<div class="col-12 col-md-4">
                    			<div class="form-group">
                    				<label for="">Carga horaria</label>
                    				<input type="text" name="carga_horaria" class="form-control" placeholder="Horas" required="" value="{{$curso->carga_horaria}}">
                    			</div>
                    		</div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="">Precio</label>
                                    <input type="number" name="precio" class="form-control" value="{{$curso->precio}}">
                                </div>
                            </div>
                    	</div>
                    	<hr>
                    	<div class="row">
                    		<div class="col-12 text-right">
	                    		<button class="btn btn-primary">
	                    			<i class="fa fa-save"></i>
	                    			Guardar
	                    		</button>
                    		</div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
	<script>
		$('.summernote').summernote({
			  height: 200,   //set editable area's height
			  codemirror: { // codemirror options
			    theme: 'monokai'
			  }
			});
	</script>
@endsection