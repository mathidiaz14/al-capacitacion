<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/gif" />
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>AL Capacitación</title>

  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&display=swap" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />

  <link href="{{asset('landing/css/style.css')}}" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="{{asset('landing/css/bootstrap.css')}}" />
  <link href="{{asset('landing/css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('landing/css/responsive.css')}}" rel="stylesheet" />
  <link href="{{asset('landing/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link rel="icon" type="image/png" href="{{asset('logo.png')}}">
  @yield('css')
</head>

<body>

  <div class="hero_area">
  	<header class="header_section ">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="{{url('/')}}">
            <span>
              <img src="{{asset('logo.png')}}" width="40px" class="mr-4">
              AL Capacitación
            </span>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="d-flex mx-auto flex-column flex-lg-row align-items-center">
              <ul class="navbar-nav  ">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('cursos')}}">Cursos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/')}}/#nosotros">Nosotros</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('blog')}}">Blog</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/')}}/#contacto">Contacto</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn_busqueda"><i class="fa fa-search"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </header>
    
    <div class='busqueda'>
      <form action="{{url('buscar')}}" method='post' class='form-horizontal'>
        @csrf
        <div class='form-group'>
              <div class='input-group'>
                <input type='text' class='form-control' id='busqueda_input' name='busqueda' placeholder='¿Qué deseas buscar?'>
              </div>
        </div>
      </form>
      <div class='col text-center btn_cerrar'>
        <a style='color:white!important;'>
          <i class='fa fa-times'></i> CERRAR
        </a>
      </div>
    </div>

    <div class='busqueda_fondo'></div>

    @yield('content')

	<!-- footer section -->
	<footer class="p-5">
		<div class="container">
		  <div class="row">
		    <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
		      <p class="text-muted small mb-4 mb-lg-0">&copy; Desarrollado por MathiasDíaz.uy</p>
		      <a href="{{url('home')}}">Administrar</a>
		    </div>
		    <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
		      <ul class="list-inline mb-0">
		        <li class="list-inline-item mr-3">
		          <a href="mailto:alcapacitacionuy@gmail.com" target="_blank">
		            <i class="fa fa-envelope fa-2x fa-fw"></i>
		          </a>
		        </li>
		        <li class="list-inline-item mr-3">
		          <a href="https://api.whatsapp.com/send?phone=+598099000814" target="_blank">
		            <i class="fab fa-whatsapp fa-2x fa-fw"></i>
		          </a>
		        </li>
		        <li class="list-inline-item mr-3">
		          <a href="https://www.facebook.com/alcapacitacion" target="_blank">
		            <i class="fab fa-facebook fa-2x fa-fw"></i>
		          </a>
		        </li>
		        <li class="list-inline-item">
		          <a href="https://www.instagram.com/al.capacitacion" target="_blank">
		            <i class="fab fa-instagram fa-2x fa-fw"></i>
		          </a>
		        </li>
		      </ul>
		    </div>
		  </div>
		</div>
	</footer>
	<!-- footer section -->


  <!-- jQery -->
  <script src="{{asset('landing/js/jquery-3.4.1.min.js')}}"></script>
  <!-- bootstrap js -->
  <script src="{{asset('landing/js/bootstrap.js')}}"></script>
  <!-- owl carousel -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <!-- custom js -->
  <script src="{{asset('landing/js/custom.js')}}"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap"></script>
  <!-- End Google Map -->
   <script>
    $(document).ready(function()
      {
        $('.btn_busqueda').click(function()
        {
          $('.busqueda').fadeIn();
          $('.busqueda_fondo').fadeIn();
          $('#busqueda_input').focus();
        });

        $('.btn_cerrar').click(function()
        {
          $('.busqueda').fadeOut();
          $('.busqueda_fondo').fadeOut();
        });

        $('.busqueda_fondo').click(function()
        {
          $('.busqueda').fadeOut();
          $('.busqueda_fondo').fadeOut();
        });     
      });
  </script>
</body>

</html>