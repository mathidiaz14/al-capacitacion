@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Nueva noticia</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/noticias')}}" class="btn btn-secondary">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/noticias')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    	@csrf
                    	<div class="row">
                    		<div class="col-12 col-lg-8">
                    			<div class="form-group">
		                    		<label for="">Titulo</label>
		                    		<input type="text" name="titulo" class="form-control" placeholder="Ingrese el titulo aquí" required="" autofocus="">
		                    	</div>
                    		</div>
                    		<div class="col-12 col-lg-4">
                    			<div class="form-group">
                    				<div class="label">Imagen</div>
                    				<input type="file" name="imagen" class="form-control" required="">
                    			</div>
                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-12">
                    			<label for="">Texto</label>
                    			<textarea name="texto" id="editor" rows="10" class="summernote form-control" required="" ></textarea>
                    		</div>
                    	</div>
                    	<hr>
                    	<div class="row">
                    		<div class="col-12 text-right">
	                    		<button class="btn btn-primary">
	                    			<i class="fa fa-save"></i>
	                    			Guardar
	                    		</button>
                    		</div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
	<script>
		$('.summernote').summernote({
			  height: 200,   //set editable area's height
			  codemirror: { // codemirror options
			    theme: 'monokai'
			  }
			});
	</script>
@endsection