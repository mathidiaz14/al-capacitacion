@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('helps.alert')
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Noticias</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/noticias/create')}}" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                                Nueva noticia
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Imagen</td>
                                    <td>titulo</td>
                                    <td>Texto</td>
                                    <td>Fecha</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($noticias as $noticia)
                                    <tr>
                                        <td>
                                            <img src="{{asset($noticia->imagen)}}" alt="" width="100px">
                                        </td>
                                        <td>{{$noticia->titulo}}</td>
                                        <td>{!!$noticia->texto!!}</td>
                                        <td>{{$noticia->created_at->format('d/m/Y H:i')}}</td>
                                        <td>
                                            <a href="{{route('noticias.edit', $noticia->id)}}" class="btn btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include('helps.delete', ['id' => $noticia->id, 'ruta' => url('admin/noticias', $noticia->id)])
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
