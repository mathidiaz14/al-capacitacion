@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('helps.alert')
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Profesores</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/profesores/create')}}" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                                Nuevo profesor
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Nombre</td>
                                    <td>Especialidad</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($profesores as $profesor)
                                    <tr>
                                        <td>
                                            <img src="{{asset($profesor->imagen)}}" alt="" width="100px" class="rounded-circle">
                                        </td>
                                        <td>{{$profesor->nombre}}</td>
                                        <td>{{$profesor->posicion}}</td>
                                        <td>
                                            <a href="{{route('profesores.edit', $profesor->id)}}" class="btn btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include('helps.delete', ['id' => $profesor->id, 'ruta' => url('admin/profesores', $profesor->id)])
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
