@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Editar Profesor</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('admin/profesores')}}" class="btn btn-secondary">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/profesores', $profesor->id)}}" class="form-horizontal col-lg-8 offset-lg-2" method="post" enctype="multipart/form-data">
                    	@csrf
                    	@method('PATCH')
	        			<div class="form-group">
	        				<div class="label">Imagen</div>
	        				<img src="{{asset($profesor->imagen)}}" alt="" width="100px">
	        				<input type="file" name="imagen" class="form-control">
	        			</div>

	                	<div class="form-group">
	                		<label for="">Nombre</label>
	                		<input type="text" name="nombre" class="form-control" value="{{$profesor->nombre}}" required="" autofocus="">
	                	</div>
	        		
	        			<div class="form-group">
	        				<div class="label">Especialidad</div>
	        				<input type="text" name="posicion" class="form-control" required="" value="{{$profesor->posicion}}">
	        			</div>
	        			<div class="form-group">
	        				<div class="label">Linked in</div>
	        				<input type="text" name="linkedin" class="form-control" value="{{$profesor->linkedin}}">
	        			</div>

                    	<hr>
                    	<div class="form-group text-right">
	            			<button class="btn btn-primary">
	                			<i class="fa fa-save"></i>
	                			Guardar
	                		</button>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection