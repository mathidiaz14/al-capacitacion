@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('helps.alert')
             <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Mi Perfil</h6>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-8 offset-md-2">
                            <form action="{{url('admin/perfil')}}" class="form-horizontal" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="">Nombre</label>
                                    <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="">Contraseña</label>
                                    <input type="password" class="form-control" name="contrasena">
                                </div>
                                <div class="form-group">
                                    <label for="">Repetir Contraseña</label>
                                    <input type="password" class="form-control" name="re_contrasena">
                                </div>
                                <div class="form-group text-right">
                                    <button class="btn btn-info">
                                        <i class="fa fa-save"></i>
                                        Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
