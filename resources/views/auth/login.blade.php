@extends('layouts.app')

@section('content')
<form method="post" action="{{route('login')}}" class="login100-form validate-form">
    @csrf
    <span class="login100-form-title p-b-70">
        Bienvenido
    </span>
    <div class="wrap-input100 validate-input  m-b-35" data-validate = "Enter email">
        <input class="input100" type="text" name="email">
        <span class="focus-input100" data-placeholder="Email"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
        <input class="input100" type="password" name="password">
        <span class="focus-input100" data-placeholder="Contraseña"></span>
    </div>

    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Ingresar
        </button>
    </div>
</form>
@endsection
