@extends('layouts.landing')

@section('content')
	<!-- slider section -->
    <section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="{{asset('images/slider-bg.jpg')}}" alt="">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1>
                  Cursos
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>

  	<section class="course_section layout_padding" id="cursos">
	    <div class="course_bg_box ">
	      <div class="bg_img_box">
	      </div>
	    </div>
	    <div class="container-fluid pr-0">
			<div class="course_container px-5">
				<div class="row my-2">
				 	@foreach($cursos as $curso)
						<div class="col-12 col-md-4 col-lg-3 my-4">
							<div class="item">
								<div class="box">
									<div class="img-box" style="height: 250px!important;">
										<img src="{{url($curso->imagen)}}" alt="">
									</div>
									<div class="detail-box">
										<h5>
											<b>{{$curso->titulo}}</b>
										</h5>
										<p>
											{!!substr($curso->descripcion, 0, 100)!!}...
										</p>
										<a href="{{url('curso', $curso->id)}}">
											Ver mas
										</a>
									</div>
								</div>
							</div>
						</div>
				 	@endforeach 
				</div>
	      	</div>
	    </div>
	  </section>
@endsection