@extends('layouts.landing')

@section('content')
	<section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="{{asset($curso->imagen)}}" alt="" width="100%" style="position:absolute; left:0; top:0; z-index: 1;">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1 style="color:#f1712a!important;">
                  {{$curso->titulo}}
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>



  <!-- about section -->

  <section class="about_section layout_padding " id="nosotros">
    <div class="container text-center">
      <div class="row">
        <div class="col-12 text-left mb-5">
          <p>
          	{!!$curso->descripcion!!}
          </p>
        </div>
        <div class="col-12 col-md-4">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
              <i class="fa fa-book m-auto fa-4x" style="color: #f1712a;"></i>
            </div>
            <h3>Modalidad</h3>
            <p class="lead mb-0">{{$curso->modalidad}}</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
              <i class="fa fa-clock m-auto fa-4x" style="color: #f1712a;"></i>
            </div>
            <h3>Carga horaria</h3>
            <p class="lead mb-0">{{$curso->carga_horaria}}</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
              <i class="fa fa-money-bill m-auto fa-4x" style="color: #f1712a;"></i>
            </div>
            <h3>Precio</h3>
            <p class="lead mb-0">
              @if($curso->precio == "0")
                Gratis
              @elseif($curso->precio > 0)
                ${{number_format($curso->precio, 0, ',', '.')}}
              @else
                Consultar
              @endif
            </p>
          </div>
        </div>
        <div class="col-12">
          <p>Temario</p>
          <div class="table table-responsive text-left">
            <table class="table table-striped">
              <tbody>
              	@foreach($temas as $tema)
              		<tr>
	                	<td>
	                  		<i class="fa fa-check mr-3" style="color: #3fc109;"></i>
	                  		{{$tema->titulo}}
	                	</td>
	              	</tr>
	            @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-12">
          @include('helps.alert')
        </div>
        <div class="col-6">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#exampleModal">
            Solicitar mas info
          </button>

          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Solicitar mas info</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{url('info')}}" class="form-horizontal text-left" method="post">
                    @csrf
                    <input type="hidden" name="curso" value="{{$curso->id}}">
                    <div class="form-group">
                      <label for="" class="label-control">
                        Nombre <b style="color:red;">*</b>
                      </label>
                      <input type="text" class="form-control" name="name" placeholder="Ingrese su nombre" required="">
                    </div>
                    <div class="form-group">
                      <label for="" class="label-control">
                        Email <b style="color:red;">*</b>
                      </label>
                      <input type="email" class="form-control" name="email" placeholder="Ingrese su email" required="">
                    </div>
                    <div class="form-group">
                      <label for="" class="label-control">
                        Teléfono
                      </label>
                      <input type="number" class="form-control" name="phone" placeholder="Ingrese su telefono">
                    </div>
                    <div class="form-group">
                      <label for="">Comentario <b style="color:red;">*</b></label>
                      <textarea id="" cols="30" rows="4" class="form-control" name="message" placeholder="Ingrese su comentario" required=""></textarea>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-6">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                          <i class="fa fa-times"></i>
                          Cerrar
                        </button>
                      </div>
                      <div class="col-6 text-right">
                        <button class="btn btn-primary">
                          <i class="fa fa-send"></i>
                          Enviar
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>  
        </div>
        <div class="col-6">
          @if($curso->precio > 0)
            <script
              src="https://www.mercadopago.com.uy/integrations/v1/web-payment-checkout.js"
              data-preference-id="<?php echo $preference->id; ?>">
            </script>
          @else
            <a href="" class="btn btn-primary btn-block disabled">
              Pagar
            </a>
          @endif
        </div>
      </div>     
    </div>
  </section>
@endsection