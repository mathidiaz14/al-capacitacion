@extends('layouts.landing')

@section('content')
    <!-- slider section -->
    <section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="images/slider-bg.jpg" alt="">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1>
                  Un lugar de aprendizaje y crecimiento personal
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>



  <!-- about section -->
  <section class="icon-section text-center p-5">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center my-5">
          <h2>Ofrecemos cursos presenciales y virtuales</h2>
        </div>
        <div class="col-lg-3">
          <div class="mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="d-flex">
              <i class="fa fa-desktop fa-4x m-auto"></i>
            </div>
            <h3 class="my-4">Tecnología</h3>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="mx-auto mb-0 mb-lg-3">
            <div class="d-flex">
              <i class="fa fa-check fa-4x m-auto"></i>
            </div>
            <h3 class="my-4">Empleabilidad</h3>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="d-flex">
              <i class="fa fa-globe fa-4x m-auto"></i>
            </div>
            <h3 class="my-4">Inglés</h3>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="mx-auto mb-0 mb-lg-3">
            <div class="d-flex">
              <i class="fa fa-futbol fa-4x m-auto"></i>
            </div>
            <h3 class="my-4">Salud y deporte</h3>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- course section start -->

  <section class="course_section layout_padding" id="cursos">
    <div class="course_bg_box ">
      <div class="bg_img_box">
        <img src="{{asset('images/course-bg.jpg')}}" alt="">
      </div>
    </div>
    <div class="container-fluid pr-0">
  		<div class="heading_container">
    		<h2>
    		  Nuestros cursos
    		</h2>
    		</div>
    		<div class="course_container">
    			<div class=" course_owl-carousel owl-carousel owl-theme ">
    			 	@foreach(App\Models\Curso::all() as $curso)
    					<div class="item">
    						<div class="box">
    							<div class="img-box" style="height: 300px!important;">
    								<img src="{{url($curso->imagen)}}" alt="">
    							</div>
    							<div class="detail-box">
    								<h5>
    									<b>{{$curso->titulo}}</b>
    								</h5>
    								<p>
    									{!!substr($curso->descripcion, 0, 100)!!}...
    								</p>
    								<a href="{{url('curso', $curso->id)}}">
    									Ver mas
    								</a>
    							</div>
    						</div>
    					</div>
    			 	@endforeach 
    			</div>
      	</div>
    </div>
    <div class="row mt-5">
        <div class="col-12 col-md-6 text-center mt-2">
          <a href="{{url('cursos')}}" class="btn btn-primary p-3">
            Ver todos los cursos
          </a>
        </div>
        <div class="col-12 col-md-6 text-center mt-2">
          <a href="http://schoology.com/login" target="_blank" class="btn btn-primary p-3" >
            Ingresar al aula virtual
          </a>
        </div>
    </div>
  </section>

  <!-- course section ends -->

  <section class="about_section layout_padding " id="nosotros">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="img-box">
            <img src="images/about-img.jpg" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="detail-box">
            <div class="heading_container">
              <h2>
                Sobre nosotros
              </h2>
            </div>
            <p>
                Somos una institución educativa que ofrece cursos y talleres en diferentes áreas.
                Realizamos cursos en distintas modalidades, presenciales in company, b-Learning y e-Learning.
                El equipo de docentes está integrado por profesionales y técnicos formados y capacitados para brindar una formación de calidad.
                Realizamos cursos y capacitaciones en todo el país.
                Somos una entidad de capacitación en convenio con INEFOP, CCE (Centros de Competitividad Empresarial) e INACOOP.
                Consulte por otros cursos específicos para su empresa o equipo de trabajo.
            </p>
            <h3>Asociados</h3>
            <div class="row">
              <div class="col-6">
                <img src="images/inefop.jfif" alt="" width="100%">
              </div>
              <div class="col-6">
                <img src="images/cce.jfif" alt="" width="100%">
              </div>
            </div>
          </div>
        </div>
        
          <div class="col-9">
            <h3>Alianzas</h3>
            <p>
                Buscando la mejor formación y capacitación en las diferentes temáticas, es que AL Capacitación encontró en <a href="www.quassure.com" target="_blank">Quassure</a> un socio de negocio.
    Quassure es una empresa que se dedica al Aseguramiento de Calidad y Pruebas de Software con amplia experiencia.
            </p>  
          </div>
          <div class="col-3">
            <img src="images/quassure.jfif" alt="" width="100%">
          </div>
        </div>
    </div>
  </section>

  <!-- end about section -->

  <!-- teacher section -->

  <section class="teacher_section layout_padding" id="equipo">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Nuestro equipo
        </h2>
      </div>
      <div class="row">
        @foreach(App\Models\Profesor::all() as $profesor)
			<div class="col-sm-6 col-lg-3">
				<div class="box">
					<div class="img-box">
						<div class="link">
							<a href="{{$profesor->linkedin}}" target="_blank"><i class="fab fa-linkedin"></i></a>
						</div>
						<img src="{{asset($profesor->imagen)}}" alt="" style="width:75%!important;">
					</div>
					<div class="detail-box">
						<h5>
							{{$profesor->nombre}}
						</h5>
						<h6>
							{{$profesor->posicion}}
						</h6>
					</div>
				</div>
			</div>
        @endforeach
      </div>
    </div>
  </section>

  <!-- end teacher section -->

  <!-- contact section -->
  <section class="contact_section " id="contacto">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 offset-md-3">
          <div class="heading_container">
            <h2>
              Contactanos
            </h2>
            <p>Nos encontramos en Canelones-Uruguay</p>
          </div>
          <div class="form_container">
            @include('helps.alert')
            <form action="{{url('contacto')}}" method="post">
            	@csrf
      				<div>
      					<input type="text" name="nombre" placeholder="Nombre" required="" />
      				</div>
      				<div>
      					<input type="email" name="email" placeholder="Email" required="" />
      				</div>
      				<div>
      					<input type="text" class="message-box" name="mensaje" placeholder="Mensaje" required="" />
      				</div>
      				<div class="btn_box">
      					<button>
      						Enviar
      					</button>
      				</div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end contact section -->

@endsection

@section('js')
 
@endsection
