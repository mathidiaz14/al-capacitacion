@extends('layouts.landing')

@section('css')
	<style>
		.slider_section .slider_bg_box::before {
		  content: "";
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		  z-index: 999;
		  background: -webkit-gradient(linear, left top, right top, from(rgba(196, 78, 78, 0.5)), to(rgba(196, 78, 78, 0.55)))!important;
		  background: linear-gradient(to right, rgba(196, 78, 78 0.5), rgba(196, 78, 78 0.55))!important;
		}

	</style>	
@endsection

@section('content')
	
	<section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="{{asset('images/slider-bg.jpg')}}" alt="">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1>
                	Su pago fue rechazado, pruebe nuevamente con otro método de pago.
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
@endsection

