@extends('layouts.landing')

@section('content')
	<section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="{{asset('images/slider-bg.jpg')}}" alt="">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1>
                  Su pago fue aprobado, nos pondremos en contacto al mail que proporcionaste en el pago.
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
@endsection