@extends('layouts.landing')

@section('content')

	<!-- slider section -->
    <section class="slider_section " id="top">
      <div class="slider_bg_box">
        <div class="bg_img_box">
          <img src="images/slider-bg.jpg" alt="">
        </div>
      </div>
      <div id="customCarousel" class="carousel slide" data-ride="carousel">
        <div class="container ">
          <div class="row">
            <div class="col-md-7 mx-auto">
              <div class="detail-box">
                <h1>
                  Blog
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>

	<section class="noticias">
		<div class="container mt-5">
			@foreach($noticias->sortByDesc('created_at') as $noticia)
				<div class="row py-3 my-4 border">
			        <div class="col-12 col-lg-3">
			            <img src="{{asset($noticia->imagen)}}" alt="" width="100%" />
			        </div>
			        <div class="col-12 col-lg-9 mt-4 border-left border-3" style="border-color:#f1712a!important; border-width: 2px!important;">
			            <h4>
			              <b>{{$noticia->titulo}}</b>
			            </h4>
			            <br>
			            <h6>
			            	{!!$noticia->texto!!}
			            </h6>
			        </div>
		      	</div>
			@endforeach
		</div>
	</section>
@endsection