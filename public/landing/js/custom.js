//  course section owl carousel

$(".course_owl-carousel").owlCarousel({
    autoplay: true,
    loop: true,
    margin: 5,
    autoHeight: true,
    nav: true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    autoWidth: true
});
