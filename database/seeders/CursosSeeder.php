<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CursosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curso = \App\Models\Curso::factory()
                    ->count(20)
                    ->has(
                            \App\Models\Temario::factory()
                            ->count(7)
                            ->state(function (array $attributes, \App\Models\Curso $curso) {
                                return ['curso_id' => $curso->id];
                            })
                        )
                    ->create();
    }
}
