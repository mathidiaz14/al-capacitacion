<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NoticiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Noticia::factory()
            ->count(10)
            ->create();
    }
}
