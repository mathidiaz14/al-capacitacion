<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProfesorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('profesors')->insert([
            'nombre' => "Alvaro Luraschi",
            'posicion' => 'Director',
            'imagen' => "images/alvaro.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Alberto Pacheco",
            'posicion' => 'Abogado',
            'imagen' => "images/alberto.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Carolina Luraschi",
            'posicion' => 'Teacher',
            'imagen' => "images/carolina.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Sergio Chanes",
            'posicion' => 'Psicólogo',
            'imagen' => "images/sergio.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Facundo Cáceres",
            'posicion' => 'QA Tester',
            'imagen' => "images/facundo.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Leonardo Corrales",
            'posicion' => 'QA Tester',
            'imagen' => "images/leonardo.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('profesors')->insert([
            'nombre' => "Pablo Brites",
            'posicion' => 'Fisioterapeuta',
            'imagen' => "images/pablo.jpg",  
            'linkedin' => "http://linkedin.com",  
            'created_at' => now(),
            'updated_at' => now(),
        ]);


    }
}