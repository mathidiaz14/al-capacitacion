<?php

namespace Database\Factories;

use App\Models\Curso;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Curso::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo'        =>  $this->faker->name,
            'imagen'        => "images/curso.png",
            'descripcion'   =>  $this->faker->text,
            'modalidad'     =>  $this->faker->randomElement(['virtual', 'presencial']),
            'carga_horaria' =>  $this->faker->numberBetween(60,120),
            'precio'        =>  $this->faker->numberBetween(50,100),
        ];
    }
}
