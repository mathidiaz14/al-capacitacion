<?php

namespace Database\Factories;

use App\Models\Temario;
use Illuminate\Database\Eloquent\Factories\Factory;

class TemarioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Temario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo'    =>  $this->faker->sentence,
        ];
    }
}
