<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profesor;
use Storage;
use File;

class ControladorProfesor extends Controller
{
    private $path = "profesores.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesores = Profesor::all();

        return view($this->path."index", compact('profesores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file  = $request->file('imagen');
        
        $profesor = new Profesor();

        if($file != null)
        {
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $profesor->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $profesor->nombre   = $request->nombre;
        $profesor->posicion = $request->posicion;
        $profesor->linkedin = $request->linkedin;
        $profesor->save();

        session(["exito" => "El profesor se creo correctamente"]);

        return redirect('admin/profesores');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profesor = Profesor::find($id);

        return view($this->path."edit", compact('profesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor = Profesor::find($id);
        
        $file  = $request->file('imagen');

        if($file != null)
        {
            unlink($profesor->imagen);
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $profesor->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $profesor->nombre   = $request->nombre;
        $profesor->posicion = $request->posicion;
        $profesor->linkedin = $request->linkedin;
        $profesor->save();

        session(["exito" => "El profesor se modifico correctamente"]);

        return redirect('admin/profesores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor = Profesor::find($id);

        $profesor->delete();
        
        unlink($profesor->imagen);

        session(["extio" => "El profesor se elimino correctamente."]);

        return back();
    }
}
