<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Temario;
use App\Models\Curso;


class ControladorTemario extends Controller
{
    private $path = "temario.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tema = new Temario();
        $tema->curso_id = $request->curso;
        $tema->titulo = $request->titulo;
        $tema->save();

        session(['exito' => "El tema se creo correctamente"]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = Curso::find($id);
        $temas = Temario::where('curso_id', $curso->id)->orderBy('created_at')->get();
        
        return view("cursos.temas", compact('curso', 'temas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tema = Temario::find($id);
        $tema->titulo = $request->titulo;
        $tema->save();

        session(['exito' => "El tema se modifico correctamente"]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tema = Temario::find($id);
        $tema->delete();

        session(['error' => "El tema se elimino correctamente"]);

        return back();
    }
}
