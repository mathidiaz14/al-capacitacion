<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use Storage;
use File;

class ControladorNoticia extends Controller
{
    private $path = "noticias.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::all()->sortByDesc('created_at');

        return view($this->path."index", compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file  = $request->file('imagen');
        
        $noticia = new Noticia();

        if($file != null)
        {
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $noticia->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $noticia->titulo = $request->titulo;
        $noticia->texto = $request->texto;
        $noticia->save();

        session(["exito" => "La noticia se creo correctamente"]);

        return redirect('admin/noticias');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = Noticia::find($id);

        return view($this->path."edit", compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticia = Noticia::find($id);
        
        $file  = $request->file('imagen');

        if($file != null)
        {
            unlink($noticia->imagen);
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $noticia->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $noticia->titulo          = $request->titulo;
        $noticia->texto           = $request->texto;
        $noticia->save();

        session(["exito" => "La noticia se modifico correctamente"]);

        return redirect('admin/noticias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $noticia = Noticia::find($id);

        $noticia->delete();

        unlink($noticia->imagen);

        session(["extio" => "La noticia se elimino correctamente."]);

        return back();
    }
}
