<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Curso;
use Storage;
use File;

class ControladorCurso extends Controller
{

    private $path = "cursos.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Curso::all();

        return view($this->path."index", compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file  = $request->file('imagen');
        
        $curso = new Curso();

        if($file != null)
        {
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $curso->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $curso->titulo = $request->titulo;
        $curso->descripcion = $request->descripcion;
        $curso->modalidad = $request->modalidad;
        $curso->carga_horaria = $request->carga_horaria;
        $curso->precio = $request->precio;
        $curso->save();

        session(["exito" => "El curso se creo correctamente"]);

        return redirect('admin/cursos');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Curso::find($id);

        return view($this->path."edit", compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Curso::find($id);
        
        $file  = $request->file('imagen');

        if($file != null)
        {
            unlink($curso->imagen);
            $name       = $file->getClientOriginalName();
            $nameFile   = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

            $curso->imagen = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }


        $curso->titulo          = $request->titulo;
        $curso->descripcion     = $request->descripcion;
        $curso->modalidad       = $request->modalidad;
        $curso->carga_horaria   = $request->carga_horaria;
        $curso->precio          = $request->precio;
        $curso->save();

        session(["exito" => "El curso se modifico correctamente"]);

        return redirect('admin/cursos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso = Curso::find($id);

        $curso->delete();
        
        unlink($curso->imagen);

        session(["extio" => "El curso se elimino correctamente."]);

        return back();
    }
}
