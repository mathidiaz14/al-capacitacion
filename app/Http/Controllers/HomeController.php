<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Curso;
use App\Models\Noticia;
use App\Models\Temario;
use App\Models\Pago;
use MercadoPago;
use Http;
use Hash;
use Auth;

class HomeController extends Controller
{

    private $mercadopago = "APP_USR-1489932783598462-041418-25b15111bb39c00e66a8b15af3b250f2-739526468";
    private $emailEmpresa = "alcapacitacionuy@gmail.com";
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index()
    {
        $cursos = Curso::all();

        return view("cursos.index", compact('cursos'));
    }

    public function curso($id)
    {
        $curso = Curso::find($id);
        
        $temas = Temario::where('curso_id', $curso->id)->orderBy('created_at')->get();

        if($curso->precio > 0)
        {
            MercadoPago\SDK::setAccessToken($this->mercadopago);

            $preference = new MercadoPago\Preference();

            // Crea un ítem en la preferencia
            $item               = new MercadoPago\Item();
            $item->title        = $curso->titulo;
            $item->quantity     = 1;
            $item->unit_price   = $curso->precio;
            $preference->items  = array($item);
            $preference->back_urls = array(
                "success" => url('pago/success'),
                "failure" => url('pago/failure'),
                "pending" => url('pago/pending')
            );
            $preference->auto_return = "approved";
            $preference->save();
            
            return view('landing.curso', compact('curso', 'preference', 'temas'));
        }
            
        return view('landing.curso', compact('curso', 'temas'));        
    }

    public function blog()
    {
        $noticias = Noticia::all();

        return view('landing.blog', compact('noticias'));
    }

    public function cursos()
    {
        $cursos = Curso::all();

        return view('landing.cursos', compact('cursos'));
    }

    public function contacto(Request $request)
    {
        $mj = new \Mailjet\Client('31b97ce5b8bf8a1acdc50bec7a06385f','6432b17651f82fc6b880d539801dfaaa',true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "al@mathiasdiaz.uy",
                        'Name' => "AL Capacitación"
                    ],
                    'To' => [
                        [
                            'Email' => $this->emailEmpresa,
                            'Name' => "AL Capacitación"
                        ]
                    ],
                    'Subject' => "Contacto desde la web",
                    'HTMLPart' => "
                        <h5>Contacto desde la web</h5>
                        <br>
                        <p>Nombre: ".$request->nombre."</p>
                        <p>Email: ".$request->email."</p>
                        <p>Mensaje: ".$request->mensaje."</p>
                    "
                ]
            ]
        ];
        $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
        $response->success();

        session(["exito" => "El mensaje se envio correctamente, nos pondremos en contacto a la brevedad"]);
        return redirect("/#contacto");
    }

    public function info(Request $request)
    {
        $curso = Curso::find($request->curso);

        $mj = new \Mailjet\Client('31b97ce5b8bf8a1acdc50bec7a06385f','6432b17651f82fc6b880d539801dfaaa',true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "al@mathiasdiaz.uy",
                        'Name' => "AL Capacitación"
                    ],
                    'To' => [
                        [
                            'Email' => $this->emailEmpresa,
                            'Name' => "AL Capacitación"
                        ]
                    ],
                    'Subject' => "Información de curso",
                    'HTMLPart' => "
                        <h5>Splicitud de información de un curso</h5>
                        <br>
                        <p>Curso: ".$curso->titulo."</p>
                        <p>Nombre: ".$request->name."</p>
                        <p>Email: ".$request->email."</p>
                        <p>Telefono: ".$request->phone."</p>
                        <p>Mensaje: ".$request->message."</p>
                    "
                ]
            ]
        ];
        
        $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
        $response->success();

        session(["exito" => "La consulta se envio correctamente, nos pondremos en contacto a la brevedad"]);
        
        return back();
    }

    public function pago_success(Request $request)
    {
        return view('landing.pago.aprobado');
    }
    
    public function pago_failure(Request $request)
    {
        return view('landing.pago.rechazado');
    }
    
    public function pago_pending(Request $request)
    {
        return view('landing.pago.pendiente');
    }

    public function perfil(Request $request)
    {
        $user = Auth::user();

        $user->name = $request->name;
        $user->email = $request->email;

        if($request->contrasena != null)
        {
            if($request->contrasena === $request->re_contrasena)
            {
                $user->password = Hash::make($request->contrasena);
                session(['exito' => 'La contraseña se modifico conrrectamente']);
            }else
            {
                session(['error' => 'Las contraseñas no coinciden']);
            }
        }
        
        $user->save();
        return back();
    }

    public function buscar(Request $request)
    {
        $cursos = Curso::where('titulo', 'LIKE', '%'.$request->busqueda.'%')
                        ->orWhere('descripcion', 'LIKE', '%'.$request->busqueda.'%')
                        ->get();

        return view("landing.cursos", compact('cursos'));
    }
}
