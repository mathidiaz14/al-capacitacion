<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    function temario()
    {
    	return $this->hasMany('App\Models\Temario', 'curso_id');
    }

    function pagos()
    {
    	return $this->hasMany('App\Models\Pago', 'curso_id');
    }
    
}
