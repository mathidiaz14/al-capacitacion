<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Temario extends Model
{
    use HasFactory;

    function curso()
    {
    	return $this->belongsTo('App\Models\Curso');
    }
}
