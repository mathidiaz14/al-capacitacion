<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControladorCurso;
use App\Http\Controllers\ControladorNoticia;
use App\Http\Controllers\ControladorPagos;
use App\Http\Controllers\ControladorProfesor;
use App\Http\Controllers\ControladorTemario;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('landing.index');
});

Auth::routes();

Route::get('/register', function () {
    return view('landing.index');
});

Route::get('/allcursos'				, [HomeController::class, 'cursos']);
Route::get('/cursos'				, [HomeController::class, 'cursos']);
Route::get('/curso/{id}'			, [HomeController::class, 'curso']);
Route::get('/blog'					, [HomeController::class, 'blog']);
Route::post('/contacto'				, [HomeController::class, 'contacto']);
Route::post('/info'					, [HomeController::class, 'info']);
Route::post('/buscar'				, [HomeController::class, 'buscar']);

Route::get('/pago/success'			, [HomeController::class, 'pago_success']);
Route::get('/pago/failure'			, [HomeController::class, 'pago_failure']);
Route::get('/pago/pending'			, [HomeController::class, 'pago_pending']);


Route::get('aprobado', function () {
    return view('landing.pago.aprobado');
});
Route::get('rechazado', function () {
    return view('landing.pago.rechazado');
});
Route::get('pendiente', function () {
    return view('landing.pago.pendiente');
});

Route::get('/home'					, [HomeController::class, 'index'])->middleware('auth')->name('home');
Route::resource('admin/cursos' 		, ControladorCurso::class)->middleware('auth');
Route::resource('admin/temario' 	, ControladorTemario::class)->middleware('auth');
Route::resource('admin/noticias' 	, ControladorNoticia::class)->middleware('auth');
Route::resource('admin/pagos' 		, ControladorPagos::class)->middleware('auth');
Route::resource('admin/profesores' 	, ControladorProfesor::class)->middleware('auth');

Route::get('admin/perfil', function () {
    return view('auth.perfil');
});

Route::post('admin/perfil'			, [HomeController::class, 'perfil']);